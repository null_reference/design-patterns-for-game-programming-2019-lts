﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Singleton
{
    public sealed class GameEnvironment
    {
        private static GameEnvironment _instance;

        private List<GameObject> _obstacles = new List<GameObject>();

        public List<GameObject> Obstacles { get { return _obstacles; } }

        public static GameEnvironment Singleton
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameEnvironment();
                    _instance.GoalLocations.AddRange(GameObject.FindGameObjectsWithTag("goal"));
                }
                return _instance;
            }
        }

        private List<GameObject> _goalLocations = new List<GameObject>();

        public List<GameObject> GoalLocations { get { return _goalLocations; } }

        public void AddObstacle(GameObject go)
        {
            _obstacles.Add(go);
        }

        public void RemoveObstacle(GameObject go)
        {
            int index = _obstacles.IndexOf(go);
            _obstacles.RemoveAt(index);
            // _obstacles.Remove(go); ??? LMAO XD
            GameObject.Destroy(go);
        }

        public GameObject GetRandomGoal()
        {
            int index = Random.Range(0, _goalLocations.Count);
            return _goalLocations[index];
        }

        // null implementation (it worked, but is different from the instructor's).
        //public GameEnvironment()
        //{
        //    _goalLocations = GameObject.FindGameObjectsWithTag("goal");
        //}
    }
}